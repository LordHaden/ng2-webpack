export interface NavbarLeftItem {
    menu_id: number;
    menu_name: string;
    menu_grade: number;
    menu_parent: number;
    status: number;
    menu_url: string;
    menu_code: string;
    menu_order: number;
    landing_flag: number;
    icon_name: string;
    icon_type: number;
    menu_display_flag: number;
}